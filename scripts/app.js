(function() {
  var app = angular.module('myApp', ['ui.router']);
  
   app.run(function($rootScope, $location, $state, LoginService) {
     console.clear();
     console.log('running');
    if(!LoginService.isAuthenticated()) {
        $state.transitionTo('login');
      }
  });
  
  app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
  function($stateProvider, $urlRouterProvider) {
    // $locationProvider.hashPrefix(''); //to remove ! mark after #

    // $urlRouterProvider.otherwise('/login');
    $stateProvider
      .state('login', {
        url : '/login',
        templateUrl : 'login.html',
        controller : 'LoginController'
      })
      .state('contact', {
        url : '/contact',
        templateUrl : 'search-contact.html',
        controller : 'myCtrl'
      });
      
       $urlRouterProvider.otherwise('/login');
  }]);
 
})();