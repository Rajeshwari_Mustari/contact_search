var app = angular.module('myApp');
 app.controller('LoginController', function($scope, $rootScope, $stateParams, $state, LoginService) {
    
    $scope.formSubmit = function() {
      if(LoginService.login($scope.username, $scope.password)) {
        $rootScope.userName = $scope.username;
        $scope.error = '';
        $scope.username = '';
        $scope.password = '';
        $state.transitionTo('contact');
      } else {
        $scope.error = "Incorrect username or password!";
      }   
    };    
  });