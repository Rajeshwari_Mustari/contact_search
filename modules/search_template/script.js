/*var app = angular.module('myApp', ['ngRoute']);
app.controller('myCtrl', function($scope) {
  $scope.firstName = "John";
  $scope.lastName = "Doe";
  console.log($scope.firstName);
});*/
var app = angular.module('myApp');
 
  app.controller('myCtrl', 
  function($scope, $rootScope, $stateParams, $state, LoginService, $http) {
    $scope.user = $rootScope.userName;
    console.log($scope.user);
    $scope.searchContact= '';

    $http({
    	url:'people_(5).json',
    	method: 'get',
    	dataType: 'json',
    	contentType: 'application/json'
    }).success(function(data){
    	console.log(data);
    	$scope.contactList = data.People;
    }).catch(function(error){
    	console.log(error);
    })

    $scope.clearSearch = function(){
    	$scope.searchContact = "";
    }

    $scope.showDetails = function(contact){
    	$scope.eachContact = contact;
    	document.getElementById("heartShapeRating").innerHTML = getRating($scope.eachContact.rating);
    }

    function getRating(rating){
    	let output = [];

  for (var i = rating; i >= 1; i--)
    output.push('<i class="fa fa-heart" aria-hidden="true" style="color: red;font-size:21px;"></i>&nbsp;');
for (let i = (5 - rating); i >= 1; i--)
    output.push('<i class="fa fa-heart-o" aria-hidden="true" style="color: red;font-size:21px;"></i>&nbsp;');

  return output.join('');

    }
    $scope.changeNavigation = function(item){
    	$state.go(item);
    }

  });